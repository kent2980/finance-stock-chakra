import { Meta, StoryObj } from "@storybook/react";
import { TopPage } from "./TopPage";

const meta: Meta = {
  title: "Page/TopPage",
  component: TopPage,
};

export default meta;

export const Default: StoryObj<typeof TopPage> = {
  args: {},
};
