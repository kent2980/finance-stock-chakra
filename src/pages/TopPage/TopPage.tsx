import { Flex } from "@chakra-ui/react";
import React from "react";
import { Logo } from "../../components/Logo/Logo";

type Props = {};

export const TopPage = (props: Props) => {
  return (
    <Flex>
      <Logo fontSize={90} />
    </Flex>
  );
};
