import type { Meta, StoryObj } from '@storybook/react';
import { fn } from '@storybook/test';
import { Button } from './Button';

// ストーリーの設定方法についての詳細はこちら: https://storybook.js.org/docs/writing-stories#default-export
const meta = {
  title: 'Example/Button', // タイトル
  component: Button, // コンポーネント
  parameters: {
    // Canvas内でコンポーネントを中央に配置するためのオプションパラメータ。詳細情報: https://storybook.js.org/docs/configure/story-layout
    layout: 'centered',
  },
  // このコンポーネントには自動生成されるAutodocsエントリがあります: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'], // タグ
  // argTypesについての詳細: https://storybook.js.org/docs/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' }, // 背景色
  },
  // `fn` を使用してonClick引数をスパイし、呼び出されるとアクションパネルに表示されます: https://storybook.js.org/docs/essentials/actions#action-args
  args: { onClick: fn() }, // 引数
} satisfies Meta<typeof Button>; // メタデータ型

export default meta; // デフォルトエクスポート
type Story = StoryObj<typeof meta>; // ストーリー型

// 引数を使ってストーリーを書く方法について: https://storybook.js.org/docs/writing-stories/args
export const Primary: Story = {
  args: {
    primary: true, // プライマリ
    label: 'Button', // ラベル
  },
};

export const Secondary: Story = {
  args: {
    label: 'Button', // ラベル
  },
};

export const Large: Story = {
  args: {
    size: 'large', // サイズ（大）
    label: 'Button', // ラベル
  },
};

export const Small: Story = {
  args: {
    size: 'small', // サイズ（小）
    label: 'Button', // ラベル
  },
};
