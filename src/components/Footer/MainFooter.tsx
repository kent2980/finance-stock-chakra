import {
  Flex,
  Link as ChakraLink,
  ListItem,
  Text,
  UnorderedList,
  VStack,
} from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";
import React from "react";
import { Logo } from "../Logo/Logo";

type ListItems = {
  name: string;
  link: string;
};

interface Props {
  listItems?: ListItems[];
}

export const MainFooter = ({ listItems }: Props) => {
  return (
    <Flex as="footer" bg="white" justifyContent="center" align="center" h={185}>
      <VStack spacing={0} justifyContent="space-between" p={4} h="100%">
        <Flex>
          <Logo fontSize={40} />
        </Flex>
        <Flex>
          <UnorderedList
            display="flex"
            flexDirection="row"
            listStyleType="none"
            p={0}
            fontSize={12}
            textColor="#515151"
          >
            {listItems?.map((item) => (
              <ListItem w={200} textAlign="center">
                <ChakraLink>{item.name}</ChakraLink>
              </ListItem>
            ))}
          </UnorderedList>
        </Flex>
        <Flex>
          <Text fontSize={10} textColor="#515151" fontWeight={500}>
            STOCUP! 2024 All rights reserved.
          </Text>
        </Flex>
      </VStack>
    </Flex>
  );
};
