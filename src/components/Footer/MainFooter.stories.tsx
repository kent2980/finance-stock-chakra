import { Meta, StoryObj } from "@storybook/react";
import { MainFooter } from "./MainFooter";

const meta: Meta = {
  title: "Layout/MainFooter",
  component: MainFooter,
};

export default meta;

export const Default: StoryObj<typeof MainFooter> = {
  args: {
    listItems: [
      { name: "サイトマップ", link: "/sitemap" },
      { name: "プライバシーポリシー", link: "/policy" },
      { name: "ご利用規約", link: "/rule" },
      { name: "お問い合わせ", link: "/customer" },
      { name: "ご利用ガイド", link: "/guide" },
    ],
  },
};
