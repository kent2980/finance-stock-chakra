import { Box, Text } from "@chakra-ui/react";
import React from "react";

interface Props {
  fontSize: number;
}

export const Logo = ({ fontSize }: Props) => {
  return (
    <Box>
      <Text
        fontSize={fontSize}
        fontFamily="Koulen"
        letterSpacing={5}
        textColor="#383A3C"
        fontWeight="bold"
      >
        STOCUP!
      </Text>
    </Box>
  );
};
