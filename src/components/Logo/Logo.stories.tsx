import { StoryObj, Meta } from "@storybook/react";
import { Logo } from "./Logo";

const meta: Meta = {
  title: "Text/Logo",
  component: Logo,
};

export default meta;

export const Default: StoryObj<typeof Logo> = {
  args: {
    fontSize: 20,
  },
};
