import { Meta, StoryObj } from "@storybook/react";
import { LoginForm } from "./LoginForm";

const meta: Meta = {
  title: "Layout/LoginForm",
  component: LoginForm,
};

export default meta;

export const Default: StoryObj<typeof LoginForm> = {
  args: {},
};
