import {
  Button,
  Flex,
  HStack,
  Icon,
  IconButton,
  Input,
  Text,
  VStack,
} from "@chakra-ui/react";
import React from "react";
import { FcGoogle } from "react-icons/fc";
import { FaSquareXTwitter } from "react-icons/fa6";
import { Logo } from "../Logo/Logo";

type Props = {};

export const LoginForm = (props: Props) => {
  return (
    <Flex height={380} width={340} backgroundColor="white">
      <VStack margin="auto" spacing={6}>
        <Logo fontSize={40} />
        <VStack width={240} spacing={4}>
          <Input
            placeholder="ユーザー名を入力してください。"
            size="xs"
            height={8}
            backgroundColor="#EFEFEF"
            borderRadius={5}
            _placeholder={{ color: "#C2C2C2" }}
          />
          <Input
            placeholder="パスワードを入力してください。"
            size="xs"
            type="password"
            height={8}
            backgroundColor="#EFEFEF"
            borderRadius={5}
            _placeholder={{ color: "#C2C2C2" }}
          />
        </VStack>
        <VStack spacing={4}>
          <Button
            colorScheme="facebook"
            textColor="white"
            size="sm"
            width={150}
            height={35}
          >
            ログイン
          </Button>
          <Button
            colorScheme="facebook"
            textColor="white"
            size="sm"
            width={150}
            height={35}
          >
            新規登録
          </Button>
        </VStack>
        <HStack spacing={5}>
          <Button variant="ghost" width={35} height={35}>
            <Icon as={FcGoogle} width={35} height={35} />
          </Button>
          <Button variant="ghost" width={35} height={35}>
            <Icon as={FaSquareXTwitter} width={35} height={35} />
          </Button>
        </HStack>
      </VStack>
    </Flex>
  );
};
