import { Meta, StoryObj } from "@storybook/react";
import { DefaultChart } from "./DefaultChart";
import sampledata from "./sampledata.json";
const meta: Meta = {
  title: "Components/DefaultChart",
  component: DefaultChart,
};

export default meta;

export const Default: StoryObj<typeof DefaultChart> = {
  args: {
    stockData: sampledata,
  },
};
