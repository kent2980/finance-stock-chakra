import React, { useEffect } from "react";
import ReactECharts from "echarts-for-react";
import * as echarts from "echarts";

// 株価データの型定義
interface StockDataItem {
  date: string;
  high: number;
  low: number;
  max: number;
  min: number;
  volume: number;
}

// DefaultChartコンポーネントのpropsの型定義
interface DefaultChartProps {
  stockData: StockDataItem[]; // 株価データの配列を受け取るprops
}

// DefaultChartコンポーネント
export const DefaultChart: React.FC<DefaultChartProps> = ({ stockData }) => {
  useEffect(() => {
    const upColor = "#00da3c"; // 上昇時の色
    const downColor = "#ec0000"; // 下落時の色
    const ROOT_PATH = "https://echarts.apache.org/examples";

    // データを整形する関数
    const splitData = (rawData: StockDataItem[]) => {
      let categoryData: string[] = [];
      let values: number[][] = [];
      let volumes: number[][] = [];
      for (let i = 0; i < rawData.length; i++) {
        categoryData.push(rawData[i].date);
        values.push([
          rawData[i].min,
          rawData[i].max,
          rawData[i].low,
          rawData[i].high,
        ]);
        volumes.push([
          i,
          rawData[i].volume,
          rawData[i].min > rawData[i].max ? 1 : -1,
        ]);
      }
      return {
        categoryData: categoryData,
        values: values,
        volumes: volumes,
      };
    };

    // 移動平均を計算する関数
    const calculateMA = (dayCount: number, data: { values: number[][] }) => {
      let result: (number | string)[] = [];
      for (let i = 0, len = data.values.length; i < len; i++) {
        if (i < dayCount) {
          result.push("-");
          continue;
        }
        let sum = 0;
        for (let j = 0; j < dayCount; j++) {
          sum += data.values[i - j][1];
        }
        result.push(+(sum / dayCount).toFixed(3));
      }
      return result;
    };

    // データを整形
    const data = splitData(stockData);

    // チャートのオプション設定
    const option = {
      animation: false,
      legend: {
        bottom: 10,
        left: "center",
        data: ["Dow-Jones index", "MA5", "MA10", "MA20", "MA30"],
      },
      // 他のオプションの設定
      series: [
        {
          name: "Dow-Jones index",
          type: "candlestick",
          data: data.values,
          itemStyle: {
            color: upColor,
            color0: downColor,
            borderColor: undefined,
            borderColor0: undefined,
          },
        },
        // 他の系列データの設定
      ],
    };

    // EChartsの初期化とオプションの設定
    const chart = echarts.init(
      document.getElementById("main") as HTMLDivElement
    );
    chart.setOption(option);
  }, [stockData]); // stockDataが変更されたときに再レンダリング

  // チャートの表示
  return <div id="main" style={{ width: "100%", height: "600px" }} />;
};

export default DefaultChart;
