# echarts-for-react

[Apache ECharts](https://github.com/apache/incubator-echarts)の最もシンプルで最高のReactラッパーです。

[![npm](https://img.shields.io/npm/v/echarts-for-react.svg)](https://www.npmjs.com/package/echarts-for-react)
[![Build Status](https://github.com/hustcc/echarts-for-react/workflows/build/badge.svg?branch=master)](https://github.com/hustcc/echarts-for-react/actions?query=workflow%3Abuild)
[![Coverage](https://img.shields.io/coveralls/hustcc/echarts-for-react/master.svg)](https://coveralls.io/github/hustcc/echarts-for-react)
[![NPM downloads](https://img.shields.io/npm/dm/echarts-for-react.svg)](https://www.npmjs.com/package/echarts-for-react)
[![License](https://img.shields.io/npm/l/echarts-for-react.svg)](https://www.npmjs.com/package/echarts-for-react)
![ECharts Ver](https://img.shields.io/badge/echarts-%5E3.0.0%20%7C%7C%20%5E4.0.0%20%7C%7C%20%5E5.0.0-blue.svg)
![React Ver](https://img.shields.io/badge/React-%20%5E15.0.0%20%7C%7C%20%20%5E16.0.0%20%7C%7C%20%20%5E17.0.0-blue.svg)


## インストール

```bash
$ npm install --save echarts-for-react

# `echarts`は`echarts-for-react`のpeerDependenceです。独自のバージョンでechartsをインストールできます。
$ npm install --save echarts
```

それから使います。

```typescript
import ReactECharts from 'echarts-for-react';

// echartsのオプションをレンダリングします。
<ReactECharts option={this.getOption()} />
```

ウェブサイトを実行できます。

```bash
$ git clone git@github.com:hustcc/echarts-for-react.git

$ npm install

$ npm start
```

その後、ブラウザで [http://127.0.0.1:8081/](http://127.0.0.1:8081/) を開いてください。またはgh-pagesで展開されている [https://git.hust.cc/echarts-for-react/](https://git.hust.cc/echarts-for-react/) を見てください。


## 使用法

シンプルなデモコードのコードは以下のとおりです。他の例はこちらをご覧ください：[https://git.hust.cc/echarts-for-react/](https://git.hust.cc/echarts-for-react/)

```typescript
import React from 'react';
import ReactECharts from 'echarts-for-react';  // または var ReactECharts = require('echarts-for-react');

<ReactECharts
  option={this.getOption()}
  notMerge={true}
  lazyUpdate={true}
  theme={"theme_name"}
  onChartReady={this.onChartReadyCallback}
  onEvents={EventsDict}
  opts={}
/>
```

バンドルサイズを減らすためにECharts.jsモジュールを手動でインポートします

**Echarts.js v5の場合:**

```typescript
import React from 'react';
// コアライブラリをインポートします。
import ReactEChartsCore from 'echarts-for-react/lib/core';
// 必要なechartsコアモジュールをインポートします。
import * as echarts from 'echarts/core';
// グラフをインポートします。すべてChart接尾辞付き
import {
  BarChart,
} from 'echarts/charts';
// コンポーネントをインポートします。すべてComponent接尾辞付き
import {
  GridComponent,
  TooltipComponent,
  TitleComponent,
} from 'echarts/components';
// レンダラーをインポートします。CanvasRendererまたはSVGRendererを導入することは必須のステップです
import {
  CanvasRenderer,
} from 'echarts/renderers';

// 必要なコンポーネントを登録します
echarts.use(
  [TitleComponent, TooltipComponent, GridComponent, BarChart, CanvasRenderer]
);

// ReactEChartsCoreの使用方法は上記と同じです。
<ReactEChartsCore
  echarts={echarts}
  option={this.getOption()}
  notMerge={true}
  lazyUpdate={true}
  theme={"theme_name"}
  onChartReady={this.onChartReadyCallback}
  onEvents={EventsDict}
  opts={}
/>
```

**Echarts.js v3またはv4の場合:**

```typescript
import React from 'react';
// コアライブラリをインポートします。
import ReactEChartsCore from 'echarts-for-react/lib/core';

// 必要なechartsモジュールを手動でインポートします。
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';

// ReactEChartsCoreの使用方法は上記と同じです。
<ReactEChartsCore
  echarts={echarts}
  option={this.getOption()}
  notMerge={true}
  lazyUpdate={true}
  theme={"theme_name"}
  onChartReady={this.onChartReadyCallback}
  onEvents={EventsDict}
  opts={}
/>
```

**Next.js**ユーザーの場合、コードのトランスパイルが必要です。

```javascript
// next.config.js
const withTM = require("next-transpile-modules")(["echarts", "zrender"]);

module.exports = withTM({})
```

## コンポーネントのプロパティ

 - **`option`** (必須, オブジェクト)

echartsのオプション構成、[https://echarts.apache.org/option.html#title](https://echarts.apache.org/option.html#title)

を参照してください。

 - **`notMerge`** (オプション, オブジェクト)

`setOption`時、データをマージしない、デフォルトは `false`。詳細は[https://echarts.apache.org/api.html#echartsInstance.setOption](https://echarts.apache.org/api.html#echartsInstance.setOption)を参照してください。

 - **`lazyUpdate`** (オプション, オブジェクト)

`setOption`時、データを遅延更新する、デフォルトは `false`。詳細は[https://echarts.apache.org/api.html#echartsInstance.setOption](https://echarts.apache.org/api.html#echartsInstance.setOption)を参照してください。

 - **`style`** (オプション, オブジェクト)

echarts divの`style`。`object`、デフォルトは {height: '300px'}。

 - **`className`** (オプション, 文字列)

echarts divの`class`。クラス名でチャートのCSSスタイルを設定できます。

 - **`theme`** (オプション, 文字列)

echartsの`theme`。`string`、使用する前に`registerTheme`する必要があります（テーマオブジェクトの形式：[https://github.com/ecomfe/echarts/blob/master/theme/dark.js](https://github.com/ecomfe/echarts/blob/master/theme/dark.js)）。例：

```typescript
// echartsをインポートします
import echarts from 'echarts';
...
// テーマオブジェクトを登録します
echarts.registerTheme('my_theme', {
  backgroundColor: '#f4cccc'
});
...
// オプション`theme`を使用してechartsをレンダリング
<ReactECharts
  option={this.getOption()}
  style={{height: '300px', width: '100%'}}
  className='echarts-for-echarts'
  theme='my_theme' />
```

 - **`onChartReady`** (オプション, 関数)

チャートが準備できたとき、`echartsオブジェクト`をパラメーターとして使用して関数をコールバックします。

 - **`loadingOption`** (オプション, オブジェクト)

echartsのローディングオプション構成、[https://echarts.apache.org/api.html#echartsInstance.showLoading](https://echarts.apache.org/api.html#echartsInstance.showLoading)を参照してください。

 - **`showLoading`** (オプション, bool, デフォルト: false)

`bool`、チャートがレンダリングされているときにローディングマスクを表示します。

 - **`onEvents`** (オプション, 配列(string=>function))

echartsイベントをバインドします。`echartsイベントオブジェクト`および`echartオブジェクト`をパラメーターとしてコールバックします。例：

```typescript
const onEvents = {
  'click': this.onChartClick,
  'legendselectchanged': this.onChartLegendselectchanged
}
...
<ReactECharts
  option={this.getOption()}
  style={{ height: '300px', width: '100%' }}
  onEvents={onEvents}
/>
```
他のイベントキー名については、こちらをご覧ください：[https://echarts.apache.org/api.html#events](https://echarts.apache.org/api.html#events)

 - **`opts`** (オプション, オブジェクト)

echartsの`opts`。`object`、`echarts.init`でechartsインスタンスを初期化する際に使用されます。ドキュメントは[こちら](https://echarts.apache.org/api.html#echarts.init)を参照してください。

```typescript
<ReactECharts
  option={this.getOption()}
  style={{ height: '300px' }}
  opts={{renderer: 'svg'}} // svgを使用してチャートをレンダリングします。
/>
```

- **`autoResize`** (オプション, boolean)

ウィンドウリサイズ時に `this.resize` をトリガーするかどうかを決定します。デフォルトは `true` です。


## コンポーネントAPI＆Echarts API

コンポーネントには `getEchartsInstance` という `1つのAPI` のみがあります。

 - **`getEchartsInstance()`** : echartsインスタンスオブジェクトを取得し、任意の `echarts API` を使用できます。

例：

```typescript
// relで下記のechartsコンポーネントをレンダリングします
<ReactECharts
  ref={(e) => { this.echartRef = e; }}
  option={this.getOption()}
/>

// この.echarts_reactを使用して `ReactECharts` を取得します

const echartInstance = this.echartRef.getEchartsInstance();
// その後、echartsの任意のAPIを使用できます。
const base64 = echartInstance.getDataURL();
```

**echartsのAPIについては** [https://echarts.apache.org/api.html#echartsInstance](https://echarts.apache.org/api.html#echartsInstance) を参照してください。

APIを使用して以下のことができます：

1. イベントのバインド / 解除
2. ダイナミックデータでの `動的チャート`
3. echarts dom / dataURL / base64を取得し、チャートをpngに保存します。
4. チャートを`解放`します。


## FAQ

### echarts 4.xを使用してチャートをsvgでレンダリングする方法

コンポーネントのプロパティ `opts` を使用して、`renderer = 'svg'` を指定します。例：

```typescript
<ReactECharts
  option={this.getOption()}
  style={{height: '300px'}}
  opts={{renderer: 'svg'}} // svgでチャートをレ

ンダリングします。
/>
```

### `Component series.scatter3D not exists. Load it first.` エラーを解決する方法

[GLインスタンス](https://www.echartsjs.com/examples/zh/index.html#chart-type-globe)を作成する場合は、[`echarts-gl`](https://www.npmjs.com/package/echarts-gl) モジュールをインストールおよびインポートします。

```sh
npm install --save echarts-gl
```

```typescript
import 'echarts-gl'
import ReactECharts from "echarts-for-react";

<ReactECharts
  option={GL_OPTION}
/>
```


## ライセンス

MIT@[hustcc](https://github.com/hustcc)