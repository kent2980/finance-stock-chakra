import React from "react";
import {
  Box,
  Flex,
  Link as ChakraLink,
  Button,
  Image,
  Input,
  VStack,
  Spacer,
  UnorderedList,
  ListItem,
  Text,
  Center,
} from "@chakra-ui/react";
import { Link as ReactRouterLink } from "react-router-dom";
import { Logo } from "../Logo/Logo";

type User = {
  name: string;
};

type MenuItems = {
  name: string;
  link: string;
};

interface Props {
  user?: User;
  onLogin?: () => void;
  onLogout?: () => void;
  onCreateAcount?: () => void;
  menuItems?: MenuItems[];
}

export const MainHeader = ({
  user,
  onLogin,
  onLogout,
  onCreateAcount,
  menuItems,
}: Props) => {
  return (
    <Box
      as="header"
      minW={720}
      fontFamily="Kosugi Maru, sans-serif"
      bg="#EAEAEA"
    >
      <VStack p={2}>
        <Flex
          width="100%"
          align="center"
          justify="space-between"
          color="black.800"
        >
          {/* ロゴ */}

          <Box>
            <ChakraLink>
              <Logo fontSize={40} />
            </ChakraLink>
          </Box>
          <Spacer />
          <Flex>
            <Input
              placeholder="銘柄コードを入力してください。"
              bg={"white"}
              fontSize={12}
              minW={220}
              mr={4}
            />
            {user ? (
              <Flex
                w={100}
                mr={4}
                bg="pink.400"
                paddingX={4}
                borderRadius={8}
                textColor="white"
                fontWeight={500}
              >
                <Center h="100%">
                  <Text>{user.name}</Text>
                </Center>
              </Flex>
            ) : (
              <Box>
                <Button
                  w={100}
                  mr={4}
                  paddingX={4}
                  bg={"#E59C85"}
                  color={"white"}
                  variant="outline"
                >
                  Login
                </Button>
              </Box>
            )}
          </Flex>
        </Flex>

        {/* メニューリンク */}
        <Flex width="100%">
          <UnorderedList
            display="flex"
            flexDirection="row"
            listStyleType="none"
            justifyContent="space-evenly"
            m={0}
            p={0}
            fontSize={12}
            textColor="#515151"
            width="100%"
          >
            {menuItems?.map((item) => (
              <ListItem textAlign="center">
                <ChakraLink>{item.name}</ChakraLink>
              </ListItem>
            ))}
          </UnorderedList>
        </Flex>
      </VStack>
    </Box>
  );
};
