import { Meta, StoryObj } from "@storybook/react";
import { MainHeader } from "./MainHeader";

// こちらで他の必要なChakra UIコンポーネントをインポートする
// import { ... } from '@chakra-ui/react';

// MainHeaderコンポーネントのStorybookメタデータを設定します。
const meta: Meta = {
  title: "Layout/MainHeader", // Storybookのサイドバーに表示されるタイトル
  component: MainHeader, // 対象となるコンポーネント
};

export default meta; // デフォルトエクスポート

const defaultMenu = [
  { name: "銘柄情報", link: "" },
  { name: "決算短信", link: "" },
  { name: "財務情報", link: "" },
  { name: "チャート", link: "" },
  { name: "テクニカル", link: "" },
  { name: "マイページ", link: "" },
]

// MainHeaderコンポーネントの各ストーリーを定義します。
export const Default: StoryObj<typeof MainHeader> = {
  args: {
    menuItems: defaultMenu,
  },
};

export const LoginUser:StoryObj<typeof MainHeader> = {
  args:{
    menuItems:defaultMenu,
    user:{
      name:"kent2980"
    }
  }
}