// .storybook/preview.tsx

import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';

// Chakra UI のプロバイダーをグローバルデコレータとして追加
const withChakra = (StoryFn: Function) => {
  return (
    <ChakraProvider>
      <StoryFn />
    </ChakraProvider>
  );
};

export const decorators = [withChakra];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  // Storybook内でChakra UIのCSSリセットを適用する
  chakra: {
    resetCSS: true,
  },
};
